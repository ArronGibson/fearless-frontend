function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
        <div class="col-md-4 mb-4">
            <div class="card shadow" style="height: 100%;">
                <img src="${pictureUrl}" class="card-img-top" style="height: 200px; object-fit: cover;">
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                    <p class="card-text">${description}</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">${starts} - ${ends}</small>
                </div>
            </div>
        </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            showErrorAlert("Failed to fetch data.");
        } else {
            const data = await response.json();

            let columnCount = 0; // Track the number of columns
            let container = document.querySelector('.row');

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);

                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const starts = new Date(details.conference.starts).toLocaleDateString('en-US');
                    const ends = new Date(details.conference.ends).toLocaleDateString('en-US');
                    const location = details.conference.location.name;

                    const html = createCard(title, description, pictureUrl, starts, ends, location);

                    // Check if a new row is needed
                    if (columnCount % 3 === 0) {
                        container.innerHTML += '<div class="row"></div>';
                    }

                    // Append the card to the last row
                    container = document.querySelector('.row:last-child');
                    container.innerHTML += html;

                    // Increment the column count
                    columnCount++;
                }
            }
        }
    } catch (e) {
        console.error(e);
        showErrorAlert("An unexpected error occured.");
    }
});

function showErrorAlert(message) {
    const alertContainer = document.querySelector('.alert-container');
    alertContainer.innerHTML = `
        <div class="alert alert-danger" role="alert">
            ${message}
        </div>
    `;
}
