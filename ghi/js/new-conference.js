window.addEventListener('DOMContentLoaded', async () => {
    // This should fetch the locations for the dropdown choices
    const locationsUrl = 'http://localhost:8000/api/locations/';
    const locationsResponse = await fetch(locationsUrl);

    if (locationsResponse.ok) {
        const locationsData = await locationsResponse.json();

        // This should add locations to dropdown
        const locationSelectTag = document.getElementById('location');
        for (let location of locationsData.locations) {
            const optionElement = document.createElement('option');
            optionElement.value = location.id;
            optionElement.innerHTML = location.name;
            locationSelectTag.appendChild(optionElement);
        }
    }

    // form submitting new conference
    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();

        // Capture form data inside the event listener
        const formData = new FormData(formTag);

        // Ensure the location field is set correctly
        const selectedLocationId = formData.get('location');

        // Check if a location is selected
        if (!selectedLocationId || selectedLocationId === 'undefined' || selectedLocationId === 'null' || selectedLocationId === '') {
            console.error('Please select a location. Value:', selectedLocationId);
            return;
        }

        const json = JSON.stringify(Object.fromEntries([...formData.entries()]));
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: 'post',
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
    
        try {
            const response = await fetch(conferenceUrl, fetchConfig);
    
            if (response.ok) {
                formTag.reset();
                const newConference = await response.json();
                console.log('Conference created:', newConference);
            } else {
                console.error('Error creating conference:', response.statusText);
            }
        } catch (error) {
            console.error('Error creating conference:', error);
        }
    });
});
