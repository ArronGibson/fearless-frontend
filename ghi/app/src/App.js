import './App.css';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';


function App(props) {
  if (!props.attendees || !Array.isArray(props.attendees)) {
    return (
      <div>
        <p>No attendees available.</p>
      </div>
    );
  }
  
  return (
    <>
      <Nav />
      <div className="container">
        { <ConferenceForm /> }
        {/* <LocationForm /> */}
        {/* <AttendeesList attendees={props.attendees} /> */}
      </div>
    </>
  );
}


export default App;
